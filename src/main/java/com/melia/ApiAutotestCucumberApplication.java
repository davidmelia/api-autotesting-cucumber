package com.melia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiAutotestCucumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiAutotestCucumberApplication.class, args);
	}
}

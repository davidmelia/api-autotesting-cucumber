package com.melia;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class DaveController {
	
	private final RestTemplate rt;
	
	public DaveController(RestTemplateBuilder rtb) {
		this.rt = rtb.build();
	}



	@GetMapping(produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public String hello() {
		String json = rt.getForObject("http://localhost:6969/someurl", String.class);
		
		return json;
	}
	
}

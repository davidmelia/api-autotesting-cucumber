package com.melia.thisdoesnotwork;

import org.junit.Test;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/notworks")
public class CucumberWiremockTestThatDoesNotWorkTest {

	@Test
	public void contextLoads() {
	}
}

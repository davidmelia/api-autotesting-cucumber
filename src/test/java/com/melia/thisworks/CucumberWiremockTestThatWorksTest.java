package com.melia.thisworks;

import org.junit.Test;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/works")
public class CucumberWiremockTestThatWorksTest {

	@Test
	public void contextLoads() {
	}
}

package com.melia.thisworks;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.any;
import static com.github.tomakehurst.wiremock.client.WireMock.anyUrl;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.melia.IntegrationTestApplication;

import cucumber.api.java.en.When;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = IntegrationTestApplication.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 6969)
public class HelloStepsPossibleSolution  {

	@Autowired
	private TestRestTemplate dao;

	@When("^the client calls /hello$")
	public void the_client_calls_hellothatworks() throws Throwable {
		stubFor(any(anyUrl()).willReturn(aResponse().withHeader("Content-Type", "application/json")
				.withStatus(200).withBody("{'dave':'melia'}")));

		String result = dao.getForObject("/hello", String.class);

		Assert.assertEquals("{'dave':'melia'}", result);
	}
}

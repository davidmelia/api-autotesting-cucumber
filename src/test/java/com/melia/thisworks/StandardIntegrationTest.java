package com.melia.thisworks;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.any;
import static com.github.tomakehurst.wiremock.client.WireMock.anyUrl;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.melia.IntegrationTestApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IntegrationTestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StandardIntegrationTest {

	@Autowired
	private TestRestTemplate dao;
	
	@Rule
	public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.wireMockConfig().port(6969));
	
    @Test
    public void the_client_calls_hello() throws Throwable {
    	wireMockRule.stubFor(any(anyUrl())
    		    .willReturn(aResponse()
    		            .withHeader("Content-Type", "application/json")
    		            .withStatus(200)
    		            .withBody("{'dave':'melia'}")));
    	
    	
     String result = dao.getForObject("/hello", String.class);
     
     Assert.assertEquals("{'dave':'melia'}", result);
    }

}

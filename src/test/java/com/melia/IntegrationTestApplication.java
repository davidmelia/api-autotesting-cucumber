package com.melia;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class IntegrationTestApplication {

	/**
	 * We have to have this because wiremock uses Jetty and Spring Boot will
	 * configure Jetty before Tomcat.
	 * 
	 * @return
	 */
	@Bean
	public TomcatEmbeddedServletContainerFactory tomcatEmbeddedServletContainerFactory() {
		return new TomcatEmbeddedServletContainerFactory();
	}
}
